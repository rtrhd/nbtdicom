# README #

Support for installation of an Orthanc dicom server on a raspberry pi at North Bristol NHS.

* R Hartley-Davies
* Jonathon Delve
* Claire Doody

Please see the [wiki](https://bitbucket.org/rtrhd/nbtdicom/wiki/) for installation instructions